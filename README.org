#+TITLE: CatiROC test gcu-abc-if

This project is part of the =CatiROC-test= [[https://gitlab.in2p3.fr/groups/CatiROC-test][group]] of projects. They all provide the
code necessary for testing the CatiROC ASIC from [[http://omega.in2p3.fr/][Omega]] in two flavours

 - Omega test board
 - Juno Mezzanine board

[[https://gitlab.in2p3.fr/CatiROC-test/gcu-abc-if][This]] project contains an specification proposal for data exchange between the
abc and the gcu. More information may be found at the [[https://catiroc-test.gitlab.io/gcu-abc-if/][page]] of the project.

* Credits

Based on [[https://github.com/thi-ng/org-spec][org-spec]], an [[http://orgmode.org][Org-mode]] template for technical specification documents
and HTML publishing ([[http://demo.thi.ng/org-spec/][example]]), under MIT License (c) by Karsten Schmidt.

* License

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the [[http://www.gnu.org/licenses/gpl.txt][GNU General Public License]]
along with this program. If not, see http://www.gnu.org/licenses/gpl.html.
